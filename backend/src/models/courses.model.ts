// courses-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
import { Application } from '../declarations';

export default function (app: Application) {
  const modelName = 'courses';
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const schema = new Schema({
    rp: String,
    seatsAvailable: Number,
    waitlist: String,
    sel: String,
    crn: Number,
    subj: String,
    crse: String,
    sec: String,
    credit: Number,
    title: String,
    addFees: Number,
    rpLimit: String,
    type:[String],
    day:[String],
    time:[String],
    nonStandardStart: [String],
    nonStandardEnd: [String],
    room: [String],
    instructor: String
  }, {
    timestamps: true
  });

  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(modelName)) {
    mongooseClient.deleteModel(modelName);
  }
  return mongooseClient.model(modelName, schema);
}
