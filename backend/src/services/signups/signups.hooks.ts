
import processSignup from '../../hooks/process-signup';
import logins from '../../hooks/logins';
import logins from '../../hooks/logins';
export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [processSignup()],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
