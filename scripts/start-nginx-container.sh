#!/bin/sh

# ASSUMES THAT THE DIRECTORIES:
#   /home/ec2-user/etc/letsencrypt
#   /home/ec2-user/var/lib/letsenctrypt
# ALREADY EXIST

docker run -d --network my-net --rm --name some-nginx \
    -v /home/ec2-user/etc/nginx/conf.d:/etc/nginx/conf.d \
    -v /home/ec2-user/etc/letsencrypt:/etc/letsencrypt \
    -v /home/ec2-user/var/lib/letsencrypt:/var/lib/letsencrypt \
    -p 80:80 -p 443:443 \
    nginx:alpine
