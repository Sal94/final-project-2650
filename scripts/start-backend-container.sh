#!/bin/sh

docker run -d --rm --network my-net --name backend \
    -v /home/ec2-user/project/backend:/mnt/backend \
    -w /mnt/backend     bkoehler/feathersjs sh -c 'npm install && npm run dev'
