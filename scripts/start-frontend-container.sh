#!/bin/sh

docker run -d --rm --network my-net --name frontend \
    -v /home/ec2-user/project/frontend:/mnt/frontend \
    -w /mnt/frontend     bkoehler/feathersjs sh -c "yarn install && sed -i \"s/'ws'/'wss'/\" ./node_modules/react-dev-utils/webpackHotDevClient.js && yarn start"
