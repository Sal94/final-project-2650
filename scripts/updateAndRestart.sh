#!/bin/bash

# any future command that fails will exit the script
set -e

docker stop  frontend backend || true && echo 'Container stopped before updating'

# Delete the old repo
sudo rm -rf /home/ec2-user/project

# BE SURE TO UPDATE THE FOLLOWING LINE WITH THE URL FOR YOUR REPO
git clone https://gitlab.com/Sal94/final-project-2650.git /home/ec2-user/project

# run the node app in a container
/home/ec2-user/project/scripts/project.sh
