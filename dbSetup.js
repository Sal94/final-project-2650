const mongoose = require("mongoose");
const mongoDB= "mongodb+srv://cpsc2650:Cpsc2650@finalproject-jpsnn.mongodb.net/cpsc2650?retryWrites=true&w=majority";
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology:true});
const db = mongoose.connection;


db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.once('open', () => {
    console.log("Connected to db");
    process.on('SIGINT', function() {
        mongoose.connection.close(function() {
            console.log('Mongoose disconnected through app termination')
            process.exit(0)
        });
    });
})

const CourseSchema = new mongoose.Schema({
    rp: String,
    seatsAvailable: String,
    waitlist: String,
    sel: String,
    crn: String,
    subj: String,
    crse: String,
    sec: String,
    credit: String,
    title: String,
    addFees: String,
    rpLimit: String,
    type:[String],
    day:[String],
    time:[String],
    nonStandardStart: [String],
    nonStandardEnd: [String],
    room: [String],
    instructor: String
    }, {
    timestamps: true
});

const Course = mongoose.model('Course', CourseSchema, 'courses')
const data = require(__dirname + '/courses.json')

data['courses'].forEach( course => {
    if ( !('end' in course) ) {
        var newCourse = new Course(course)
        newCourse.save(function (err, course) {
            if (err) {
                console.error(err);
                process.exit(1)
            }
            // console.log(course.crn + " saved to courses collection.");
        });
    } else {
        console.log('Data has been populated')
        process.exit(0)
    }
        
})