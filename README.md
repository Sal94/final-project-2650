# Term project for CPSC 2650

## Created by
1. Gursharan Singh
2. Rafael Amorim
3. Salvatore De Simone
4. Tristian Labanowich

### Want to run the site off of the current domain?
Awseome! It's super simple to set up
1. Go into scripts/preParse.sh and change the curl link to current courses url
2. run scripts/preParse.sh 
3. run node parser.js
4. copy courses.json into frontend/src/courses.json
5. edit courses.json, delete the endOfFile object
6. push your changes and the website will be updated!


### Want to run it off you're own domain? 
No problem there will just be a few things to set up
**Please note this is for users using aws**
#### Set up Google cloud console
1. Go to google cloud console and set up a new CNAME for you domian
> - Click on the hamburger menu and click the link under Networking -> Netwrok services -> cloudDNS
> - Select Your Zone
> - click *add record set*
> - name your DNS name 

2. go to aws search under services for ec2
3. find your *running instance* and copy the ***Public DNS (IPv4)*** link 
> - On the side bar go to Netowork and Security -> security Groups
> - Select your machine
> - select *edit inbound rules*
> - add ***Two rules Each*** for *HTTP* and *HTTPS*
> - in the text box beside source enter **0.0.0.0/0** and **::/0** for both *HTTP* and *HTTPS*
4. paste that link into the *Canonical name* section in **google cloud console**
5. save changes


#### Change file configurations
1. Navigate to etx/nginx/conf.d
2. Edit each file changing *project.494918.xyz* to ***Your DNS that you set up in google cloud console***
3. Navigate to frontend/src/feathers.js, frontend/src/pages/Login/index.js, frontend/src/components/searchForm.js
4. Do the same as **Step 2**

#### setting up for certbot
1. Copy each file from etc/nginx/conf.d to ~/etc/nginx/conf.d
> *Note you may need root permissions to do this if you do not have such permsions you will have to copy the files one by one using* ***sudo***
2. Copy over your own options to ~/etc/nginx/conf.d or use the ones we have provided under etc/additionalFiles

#### Running certbot
1. run scripts/certbot.sh sh
> *Please note you may need to modify permissions*
2. Press 1 to spin up a *temporary web server*
3. Enter your email
4. Agree to terms and conditions and whether you would like to share your email
5. enter your ***Domain*** that you set up in *Google cloud console*
At this point your domain should be certified

#### Setting up docker network
1. Run docker network create my-net
2. run docker network ls to make sure it was created

#### final script changes
1. Navigate to the scripts folder
2.  in both *start-backend-container.sh* and *start-frontend-container.sh* change the volume from /home/ec2-user/projectTest/backend:/mnt/backendTest \ to $PWD/backend:/mnt/backendTest \
3.  run scripts/start-backend-container.sh
4.  run scripts/start-frontend-container.sh
> Note: you will need to give theese scripts time to install if you want to be sure they are ready change *docker run -d* to *docker run -it*
> Additionally you may encounter issues if the machine you are using does not have enough resources, to fix this upgrade your machine to have the allocated resources
5. run run scripts/start-nginx-container.sh

### Check to see your app is running at your domain

