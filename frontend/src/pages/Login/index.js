import React, { useState, useEffect } from 'react';
import { loadReCaptcha, ReCaptcha } from 'react-recaptcha-v3'
import styled from "styled-components";
import client from "./../../feathers";

export default function Login() {
  const [errorMsg, setErrorMsg] = useState('');
  const [captchaPassed, setCaptchaPassed] = useState(true);
  
  useEffect(() =>{
    loadReCaptcha('6LfA-uUUAAAAAM8MrrgKNmiE5ruCYlwyI8s1PIwU');
  },[]);
  
  const handleEnterGuest = e => {
    e.preventDefault();
    window.location.href = 'https://project.494918.xyz/';
  }
    
  const verifyCallback = (recaptchaToken) => {
    // Here you will get the final recaptchaToken!!!  
    // Connect to the `signups` service
    const signups = client.service('signups');
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var form = document.getElementById('mainForm')
    var loginButton = document.getElementById('login')
    var alertBox = document.getElementById('alert-box');
    
    form.addEventListener('submit', function (event) {
      if (form.checkValidity()) {
        signups.find({
          query: {
            email: document.getElementById('email').value,
          }
        })
        .then(response => {
          if (response.total > 0) {
            setErrorMsg('There is already a registered user with this email.');
            alertBox.classList.remove('d-none');
            alertBox.removeAttribute("tabindex");
          } else {
            signups.create({
              email: document.getElementById('email').value,
              password: document.getElementById('password').value,
              token: recaptchaToken
            })
            .then(function() {
              console.log('service created in db');
              window.location.href = `https://project.494918.xyz/?user`;
            })
            .catch(function() {
              setCaptchaPassed(false);
            });
          }
        })
        .catch( error => console.log(error));
        
        event.preventDefault();
        event.stopPropagation();
      }
    }, false);
    
    loginButton.addEventListener('click', function (event) {
       if (form.checkValidity()) {
        signups.find({
          query: {
            email: document.getElementById('email').value,
            password: document.getElementById('password').value,
          }        
        })
        .then(response => {
          if (response.total !== 1) {
            setErrorMsg('No user registered with this email and password.');
            alertBox.classList.remove('d-none');
            alertBox.removeAttribute("tabindex");
          } else {
            window.location.href = `https://project.494918.xyz/?user`;
          }
        })
        .catch(error => console.log(error));
       }
      event.preventDefault();
      event.stopPropagation();
    }, false);
    
  }
  
  // render() {
    return (
      <LoginContainer className="container">
        <h1>Langara Course Scheduler</h1>
        <div className="row">
          <div className="col-sm-9 col-md-8 col-lg-5 mx-auto">
            <div className="card my-5">
              <div className="card-body">
                <form className="needs-validation" id='mainForm' aria-labelledby='form-title'>
                  <h4 id="form-title" className="card-title text-center">Sign In</h4>
                  <div id="alert-box" className="alert alert-danger text-center d-none" role="alert" tabindex="-1">{errorMsg}</div>
                  <div>
                    <label htmlFor="email">Email*</label>
                    <input
                      type="email"
                      id="email"
                      className="form-control"
                      placeholder="Type your email"
                      aria-required="True"
                      required
                    />
                    <div className="invalid-feedback" role="alert"> Please enter a valid email address.</div>
                  </div>
  
                  <div className="my-2">
                    <label htmlFor="password">Password*</label>
                    <input
                      type="password"
                      id="password"
                      className="form-control"
                      placeholder="Type your password"
                      aria-required="True"
                      required
                    />
                    <div className="invalid-feedback" role="alert"> Please enter a valid password.</div>
                  </div>
                  <button
                    className="btn btn-lg btn-block text-uppercase my-4"
                    type="submit"
                  >
                    Sign up
                  </button>
                  <div className={ captchaPassed ? "alert alert-danger d-none": "alert alert-danger"} role="alert" id="captchaMessage"> Failed reCAPTCHA </div>
                  <hr className="my-4" />
                </form>
                 <button
                    className="btn btn-lg btn-block text-uppercase my-4"
                    id="login"
                  >
                    Log in
                </button>
                 <button
                    className="btn btn-lg btn-block text-uppercase my-4"
                    id="login-guest"
                    onClick={handleEnterGuest}
                  >
                    Enter as a Guest
                </button>
              </div>
            </div>
          </div>
          <ReCaptcha
              sitekey="6LfA-uUUAAAAAM8MrrgKNmiE5ruCYlwyI8s1PIwU"
              action='signups'
              verifyCallback={verifyCallback}
          />
        </div>
      </LoginContainer>
)
  // };
};
const LoginContainer = styled.div`
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  
  h1 {
    color: #ffffff;
    text-align: center;
    font-weight: 400;
    font-size: 4em;
  }

  button {
    background-color: #f15a22 !important;
    color: #ffffff !important;
    transition: all 450ms ease-in-out;
  }

  button:hover {
    background-color: #e4490f !important;
    transform: scale(1.005);
  }

  .card {
    box-shadow: 2px 2px 20px 5px rgba(0, 0, 0, 0.3);
  }
  
  label{
    text-transform: uppercase;
    font-weight: 700;
    color: #444;
  }
`;
