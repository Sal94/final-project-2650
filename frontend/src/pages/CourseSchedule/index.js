import React, { useState } from "react";

import styled from "styled-components";

import Calendar from "../../components/calendar";
import NavBar from "../../components/navbar";
import SearchForm from "../../components/searchForm";

export default function CourseSchedule() {
  return (
    <>
      <NavBar />
      <CourseScheduleContainer className="container-fluid w-100">
        <div className="row">
          <div className="col-12 col-md-8 col-lg-6 mx-auto p-0">
            <SearchForm />
          </div>
          <div className="col-12 col-md-8 col-lg-6 mx-auto p-0">
            <Calendar />
          </div>
        </div>
      </CourseScheduleContainer>
    </>
  );
}

const CourseScheduleContainer = styled.div`
  max-width: 2000px;
  min-height: 100vh;
  background: #fff;
  .row {
    padding: 0 1rem;
  }
  // background-color: var(--color-langara-orange-light);
`;
