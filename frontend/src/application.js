import React, { Component } from "react";

import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import CourseSchedule from "./pages/CourseSchedule";
import Login from "./pages/Login";
import client from "./feathers";

class Application extends Component {
  render() {
    return (
      <div className="main-container">
        <Router>
          <Switch>
            <Route path="/" exact component={CourseSchedule} />
            <Route path="/login" exact component={Login} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default Application;