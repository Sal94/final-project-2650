import React, { useState, useEffect, useContext, useRef } from "react";
import * as moment from "moment";
//importing required css
import "@syncfusion/ej2-base/styles/material.css";
import "@syncfusion/ej2-buttons/styles/material.css";
import "@syncfusion/ej2-calendars/styles/material.css";
import "@syncfusion/ej2-dropdowns/styles/material.css";
import "@syncfusion/ej2-inputs/styles/material.css";
import "@syncfusion/ej2-lists/styles/material.css";
import "@syncfusion/ej2-navigations/styles/material.css";
import "@syncfusion/ej2-popups/styles/material.css";
import "@syncfusion/ej2-splitbuttons/styles/material.css";
import "@syncfusion/ej2-react-schedule/styles/material.css";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronCircleLeft,
  faChevronCircleRight,
} from "@fortawesome/free-solid-svg-icons";

import {
  ScheduleComponent,
  Day,
  Week,
  Month,
  Inject,
  ViewsDirective,
  ViewDirective,
} from "@syncfusion/ej2-react-schedule";
import { DataManager } from "@syncfusion/ej2-data";

import styled from "styled-components";
import {
  GlobalStateContext,
  GlobalDispatchContext,
} from "../context/GlobalContextProvider";

const Calendar = () => {
  const scheduleObj = useRef(null);
  const state = useContext(GlobalStateContext);
  const dispatch = useContext(GlobalDispatchContext);
  const [data, setData] = useState([]);
  const [selectedSectionData, setSelectedSectionData] = useState([]);
  const [localSelected, setLocalSelected] = useState([]);
  const dataManager = new DataManager([...data, ...selectedSectionData]);
  const { sections } = state;

  const getDate = (day, time) => {
    var dayNum = getDayNum(day);
    var date = moment()
      .hour(time.substr(0, 2))
      .minute(time.substr(2, 2))
      .day(dayNum)
      .format();
    return date;
  };
  const getDayNum = (day) => {
    switch (day.charAt(0)) {
      case "M":
        return 1;
      case "T":
        return 2;
      case "W":
        return 3;
      case "R":
        return 4;
      case "F":
        return 5;
      case "S":
        return 6;
      default:
        return 0;
    }
  };

  const isSlotAvailable = (day, startTime, endTime) => {
    startTime = new Date(startTime);
    endTime = new Date(endTime);
    if (data.length === 0) return true;
    var conflict = false;
    data.map((course) => {
      let courseStartTime = new Date(course.StartTime);
      let courseEndTime = new Date(course.EndTime);
      if (
        courseStartTime.getTime() < endTime.getTime() &&
        courseEndTime.getTime() > startTime.getTime()
      ) {
        conflict = true;
        dispatch({
          type: "ADD_TO_ERROR",
          payload: {
            type: "conflict",
            message: `Time conflict with ${course.Subject}`,
          },
        });
      } else {
        dispatch({ type: "ADD_TO_ERROR", payload: { type: "", message: "" } });
      }
    });
    return !conflict;
  };

  const nextSection = () => {
    var sectionIndex = state.selectedSection;
    sectionIndex = (sectionIndex + 1) % sections.length;
    dispatch({
      type: "UPDATE_SELECTED_SECTION",
      payload: sectionIndex,
    });
  };
  const previousSection = () => {
    var sectionIndex = state.selectedSection;
    sectionIndex = (sectionIndex - 1) % sections.length;
    if (sectionIndex < 0) sectionIndex = sections.length - 1;
    dispatch({
      type: "UPDATE_SELECTED_SECTION",
      payload: sectionIndex,
    });
  };

  const getCourseData = (course) => {
    var tempData = [];
    var newCourse = course;
    var conflict = false;

    if (typeof newCourse === "undefined") return; // return on initial render i.e no courses added on initial render
    if (typeof newCourse.crn === "undefined") return; // return on initial render i.e no courses added on initial render

    var Id = newCourse.crn;
    var Subject = `${newCourse.subj} ${newCourse.crse} ${newCourse.sec}`;
    var time = newCourse.time;
    var location = newCourse.room;
    var Description = `CRN: ${newCourse.crn}`;
    var days = newCourse.day.map((item) => item.replace(/-/g, ""));
    days.map((day, i) => {
      day.split("").map((daySingle) => {
        var tempTime =
          typeof time[i] === "undefined" ? time[time.length - 1] : time[i];
        var Location =
          typeof location[i] === "undefined"
            ? location[location.length - 1]
            : location[i];
        var StartTime = getDate(daySingle, tempTime.substr(0, 4));
        var EndTime = getDate(daySingle, tempTime.substr(5, 4));
        if (isSlotAvailable(daySingle, StartTime, EndTime)) {
          tempData.push({
            Id,
            Subject,
            StartTime,
            EndTime,
            Location,
            Description,
            daySingle,
            RecurrenceRule: "FREQ=WEEKLY;INTERVAL=1;COUNT=10",
          });
        } else {
          conflict = true;
        }
      });
    });
    if (!conflict) return tempData;
    else {
      conflict = false;
    }
  };

  useEffect(() => {
    var tempData = getCourseData(sections[state.selectedSection]);
    if (typeof tempData !== "undefined") setSelectedSectionData([...tempData]);
  }, [state.selectedSection]);

  useEffect(() => {
    setSelectedSectionData([]);
    var tempData = getCourseData(state.newCourse);
    if (typeof tempData !== "undefined") {
      setData([...data, ...tempData]);
      dispatch({ type: "UPDATE_SELECTED", payload: state.newCourse });
    }
  }, [state.newCourse]);

  useEffect(() => {
    if (localSelected.length > state.selected) {
      var tempData = [];
      state.selected.map((course) => {
        let temp = getCourseData(course);
        if (typeof temp !== "undefined") tempData = [...tempData, ...temp];
      });
      setData([...tempData]);
    }
    setLocalSelected([...state.selected]);
  }, [state.selected]);

  return (
    <CalindarContainer>
      <div className="section-selector text-center row my-4">
        <h3 className="col-12">Change Sections</h3>
        <div className="col-4 col-md-3 mx-auto d-flex justify-content-between align-items-center icons">
          <FontAwesomeIcon
            icon={faChevronCircleLeft}
            onClick={previousSection}
            aria-label='See previous schedule result'
          />
          {sections.length > 0 && (
            <span>{`${state.selectedSection + 1} of ${sections.length}`}</span>
          )}
          <FontAwesomeIcon icon={faChevronCircleRight} onClick={nextSection} aria-label='See next schedule'/>
        </div>
      </div>
      <ScheduleComponent
        ref={scheduleObj}
        cssClass="scheduler"
        height="100%"
        width="100%"
        readonly={true}
        rowAutoHeight={true}
        eventSettings={{
          dataSource: dataManager,
          fields: {
            id: "Id",
            subject: { name: "Subject" },
            isAllDay: { name: "IsAllDay" },
            startTime: { name: "StartTime" },
            endTime: { name: "EndTime" },
            location: { name: "Location" },
            description: { name: "Description" },
          },
        }}
      >
        <ViewsDirective>
          <ViewDirective option="Week" startHour="08:00" endHour="22:00" />
          <ViewDirective option="Month" />
          <ViewDirective option="Day" />
        </ViewsDirective>
        <Inject services={[Day, Week, Month]} />
      </ScheduleComponent>
    </CalindarContainer>
  );
};
const CalindarContainer = styled.div`
  min-height: 600px;
  .section-selector {
    .icons {
      font-size: 1.3rem;
      svg {
        &:hover {
          cursor: pointer;
        }
      }
    }
  }
  width: 100%;
  .e-schedule-toolbar .e-toolbar-right .e-add {
    display: none !important;
  }
  .e-schedule-toolbar .e-toolbar-right .e-today .e-tbar-btn-text {
    display: inline !important;
  }
  .scheduler {
    height:75vh !important;
    min-height:330px;
    .e-appointment{
      width:100% !important;
      height: auto !important;
    }
    .e-week-view{
        .e-time-cells-wrap {
          table {
            tbody {
              tr {
                height: 2vh !important;
                min-height:10px;
                overflow: visible;
                position: relative;
                .e-time-cells{
                  position: absolute;
                  width:100%;
                  
                }
                td {
                  height: 2vh !important;
                  min-height:10px;
                  position: relative;
                  overflow: visible;
                  font-size: 2vh;
                  
                  span {
                    position: absolute;
                    top: 5px;
                    left: 5px;
                    z-index: 10;
                    
                  }
                }
              }
            }
          }
        }
        .e-content-wrap {
          height: 75vh !important;
          table {
            tbody {
              height: 75vh;
              tr {
                height: 2vh !important;
                min-height:10px;
                td {
                  height: 2vh !important;
                  min-height:10px;
                }
              }
            }
          }
        }
      }
    }
  }
`;

export default Calendar;
