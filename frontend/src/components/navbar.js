import React, {useState, useEffect} from "react";

import { faSignOutAlt, faSignInAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import styled from "styled-components";

export default function NavBar() {
  
  const [btnText, setBtnText] = useState('');
  const [btnType, setBtnType] = useState('');
  
  useEffect(() => {
    chekURLParams();
  }, []);
  
  const chekURLParams = () => {
    let params = window.location.search;
    
    if(params){
      setBtnText('Logout');
      setBtnType(faSignOutAlt);
    }else{
      setBtnText('Login');
      setBtnType(faSignInAlt);
    }
  }
  
  return (
    <NavContainer className="navbar navbar-expand-lg sticky-top">
      <a id="logo" className="navbar-brand" href="/" aria-label="Go to login page">
        Course Scheduler
      </a>

      <ul className="navbar-nav ml-auto">
        <li className="nav-item">
          <button type="button">
            {<FontAwesomeIcon icon={btnType} aria-label={btnText} role="button" />}
            <a className="nav-link" href="/login" aria-label={btnText}>
              {btnText}
            </a>
          </button>
        </li>
      </ul>
    </NavContainer>
  );
}

const NavContainer = styled.nav`
  height: 60px;
  padding: 0 10vw;
  display: flex;
  justify-content: space-between;
  color: #ffffff;
  align-items: center;
  box-shadow: 0 1px 10px rgba(0, 0, 0, 0.3);
  background-color: var(--color-langara-orange);
  a {
    text-decoration: none;
    color: #ffffff;
  }

  #logo {
    font-size: 2em;
    transition: all 300ms ease-in-out;
  }
  
  #logo:hover {
    text-shadow: 0 0 10px rgba(0,0,0,0.4);
  }

  .logout a {
    font-size: 1.3em;
  }
  
  .nav-item button{
    background: none;
    display: flex;
    align-items: center;
    color: #FFF;
    margin: 0 2px;
    border: 0;
  }
  
  .nav-item button a{
    color: #FFF;
  }
`;
