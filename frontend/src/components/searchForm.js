import React, { useState, useEffect, useContext } from "react";
import styled from "styled-components";
import client from "../feathers";

import {
  GlobalStateContext,
  GlobalDispatchContext,
} from "../context/GlobalContextProvider";

import data from "../courses.json";

import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function SearchForm() {
  const state = useContext(GlobalStateContext);
  const dispatch = useContext(GlobalDispatchContext);
  const { selected, sections } = state;
  const [subjects, setSubjects] = useState([]); // all subjects
  const [courses, setCourses] = useState([]); // all courses realted to specific subject

  //change courses according to selected subject
  const handelSubjectChange = (e) => {
    var courses = [...data.data];
    setCourses(courses.filter((item) => item.subj === e.target.value));
  };

  //function to get course list with dublicates removed
  const getCoursesList = () => {
    var temp = courses.map((course) => {
      return {
        subj: course.subj,
        crse: course.crse,
        title: course.title,
      };
    });
    temp = temp.filter(
      (item, index, self) =>
        index ===
        self.findIndex(
          (t) =>
            t.sub === item.sub && t.crse === item.crse && t.title === item.title
        )
    );
    return temp;
  };

  const handelCourseChange = (e) => {
    var sections = courses.filter((item) => item.crse == e.target.value);
    dispatch({
      type: "UPDATE_SECTIONS",
      payload: [...sections],
    });
  };

  const handelSectionChange = (section, i, e) => {
    var elements = document.querySelectorAll(".table-row-sections");
    for (let i = 0; i < elements.length; i++) {
      elements[i].classList.remove("selected");
    }
    e.target.parentElement.classList.add("selected");
    dispatch({
      type: "UPDATE_SELECTED_SECTION",
      payload: i,
    });
  };

  const removeSection = (i) => {
    dispatch({
      type: "REMOVE_FROM_SELECTED",
      payload: selected[i],
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (state.selectedSection === null) {
      document.querySelector("#warning").classList.remove("d-none");
      document.querySelector("#warning").innerHTML =
        "Please choose correct options";
      return;
    } else {
      document.querySelector("#warning").classList.add("d-none");
      if (state.error.type !== "conflict") {
        dispatch({
          type: "UPDATE_NEW_COURSE",
          payload: { ...sections[state.selectedSection] },
        });
        dispatch({
          type: "UPDATE_SECTIONS",
          payload: [],
        });
        dispatch({
          type: "UPDATE_SELECTED_SECTION",
          payload: null,
        });

        setCourses([]);

        e.target.reset();
      }
    }
  };

  useEffect(() => {
    var subjs = data.data.map((item) => {
      return item.subj;
    });
    subjs = subjs.filter(
      (item, index, self) => index === self.findIndex((t) => t === item)
    );
    subjs = subjs.filter((item) => item.trim() && item !== "");
    setSubjects([...subjs]);
  }, []);

  useEffect(() => {
    if (state.error.type === "conflict") {
      document.querySelector("#warning").classList.remove("d-none");
      document.querySelector("#warning").innerHTML = state.error.message;
      if (state.selectedSection === null)
        dispatch({ type: "REMOVE_LAST_SELECTED" });
    } else {
      document.querySelector("#warning").classList.add("d-none");
    }
  }, [state.error]);

  return (
    <FormContainer>
      <h3>Search for courses</h3>

      <form onSubmit={handleSubmit} className="my-4">
        <div className="form-row">
          <div
            className="col-8 mx-auto text-center my-2 d-none"
            id="warning"
            role="alert"
          >
            Please choose correct options
          </div>
          <div className="col-8 col-md-6 ">
            <label htmlFor="subjectSelector" className="my-2">
              Select Subject :
            </label>
            <select
              className="custom-select mr-sm-2"
              id="subjectSelector"
              onChange={handelSubjectChange}
              defaultValue="choose..."
              aria-required="True"
            >
              <option value="choose...">Choose...</option>
              {subjects.map((subj, i) => (
                <option key={i} value={subj} role="option">
                  {subj}
                </option>
              ))}
            </select>
          </div>
          <div className="col-8 col-md-6 ">
            <label htmlFor="courseSelector" className="my-2">
              Select Course:
            </label>
            <select
              className="custom-select mr-sm-2"
              id="courseSelector"
              onChange={handelCourseChange}
              defaultValue="choose..."
            >
              <option value="choose...">Choose...</option>
              {getCoursesList().map((course, i) => (
                <option key={i} value={course.crse} role="option">
                  {`${course.subj} ${course.crse} : ${course.title}`}
                </option>
              ))}
            </select>
          </div>
          <div className="col-12 p-0">
            {sections.length > 0 ? (
              <>
                <label className="my-2">Select Secion:</label>
                <div className="table-responsive">
                  <table className="table table-hover table-striped table-sm ">
                    <thead>
                      <tr>
                        <th scope="col">RP</th>
                        <th scope="col">Seats Avail</th>
                        <th scope="col"># on Waitlist</th>
                        <th scope="col">Sel</th>
                        <th scope="col">CRN</th>
                        <th scope="col">Subj</th>
                        <th scope="col">Crse</th>
                        <th scope="col">Sec</th>
                        <th scope="col">Cred</th>
                        <th scope="col">Title</th>
                        <th scope="col"> Add'l Fees</th>
                        <th scope="col">Rpt Limit</th>
                        <th scope="col">Type</th>
                        <th scope="col">Day(s)</th>
                        <th scope="col">Time</th>
                        <th scope="col">NonStandard Start/End</th>
                        <th scope="col">Room</th>
                        <th scope="col">Instructor(s)</th>
                      </tr>
                    </thead>
                    <tbody>
                      {sections.map((section, i) => {
                        return (
                          <tr
                            className="table-row-sections"
                            onClick={(e) => handelSectionChange(section, i, e)}
                            key={i}
                          >
                            <td>{section.rp}</td>
                            <td>{section.seatsAvailable}</td>
                            <td>{section.waitlist}</td>
                            <td>{section.sel}</td>
                            <td>{section.crn}</td>
                            <td>{section.subj}</td>
                            <td>{section.crse}</td>
                            <td>{section.sec}</td>
                            <td>{section.credit}</td>
                            <td className="text-nowrap">{section.title}</td>
                            <td>{section.addFees}</td>
                            <td>{section.rpLimit}</td>
                            <td>{section.type}</td>
                            <td className="text-nowrap">
                              {section.day.map((item, i) => (
                                <React.Fragment key={i}>
                                  <span>{item}</span>
                                  <br />
                                </React.Fragment>
                              ))}
                            </td>
                            <td>
                              {section.time.map((item, i) => (
                                <React.Fragment key={i}>
                                  <span className="text-nowrap">{item}</span>
                                  <br />
                                </React.Fragment>
                              ))}
                            </td>
                            <td className="text-nowrap">
                              {section.nonStandardStart.map((item, i) => (
                                <React.Fragment key={i}>
                                  <span>{item}</span>
                                  <br />
                                </React.Fragment>
                              ))}
                            </td>

                            <td>
                              {section.room.map((item, i) => (
                                <React.Fragment key={i}>
                                  <span>{item}</span>
                                  <br />
                                </React.Fragment>
                              ))}
                            </td>
                            <td className="text-nowrap">
                              {section.instructor}
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </table>
                </div>
              </>
            ) : (
              ""
            )}
          </div>
        </div>
        <button className="btn btn-primary my-4" type="submit">
          Submit
        </button>
      </form>

      {selected.length > 0 ? (
        <div className="w-100 table-responsive">
          <h4>Selected Courses:</h4>
          <table className="table table-striped table-sm">
            <thead>
              <tr>
                <th scope="col">CRN</th>
                <th scope="col">Subj</th>
                <th scope="col">Crse</th>
                <th scope="col">Sec</th>
                <th scope="col">Title</th>
                <th scope="col">Type</th>
                <th scope="col">Day(s)</th>
                <th scope="col">Time</th>
                <th scope="col">Room</th>
                <th scope="col">Instructor(s)</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              {selected.length > 0 ? (
                selected.map((course, i) => (
                  <tr key={i}>
                    <td>{course.crn}</td>
                    <td>{course.subj}</td>
                    <td>{course.crse}</td>
                    <td>{course.sec}</td>
                    <td className="text-nowrap">{course.title}</td>
                    <td>{course.type}</td>
                    <td className="text-nowrap">
                      {course.day.map((item, i) => (
                        <React.Fragment key={i}>
                          <span>{item}</span>
                          <br />
                        </React.Fragment>
                      ))}
                    </td>
                    <td>
                      {course.time.map((item, i) => (
                        <React.Fragment key={i}>
                          <span className="text-nowrap">{item}</span>
                          <br />
                        </React.Fragment>
                      ))}
                    </td>

                    <td>
                      {course.room.map((item, i) => (
                        <React.Fragment key={i}>
                          <span>{item}</span>
                          <br />
                        </React.Fragment>
                      ))}
                    </td>
                    <td className="text-nowrap">{course.instructor}</td>
                    <td
                      className="text-nowrap remove"
                      onClick={() => removeSection(i)}
                    >
                      {
                        <FontAwesomeIcon
                          icon={faTimes}
                          aria-label="Remove selected course"
                          role="button"
                        />
                      }
                    </td>
                  </tr>
                ))
              ) : (
                <tr></tr>
              )}
            </tbody>
          </table>
        </div>
      ) : (
        ""
      )}
    </FormContainer>
  );
}

const FormContainer = styled.div`
  padding: 30px;
  #warning {
    border: 1.5px solid #dc3545;
    border-radius: 5px;
    color: #dc3545;
    padding: 0.5rem;
  }
  label {
    font-size: 1rem;
    font-weight: bold;
  }
  table {
    .remove {
      &:hover {
        cursor: pointer;
      }
    }
  }
  .table-row-sections {
    &:hover {
      cursor: pointer;
    }
    &.selected {
      background: rgba(0, 0, 0, 0.3);
    }
  }
`;
