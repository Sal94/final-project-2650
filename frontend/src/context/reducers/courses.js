export const coursesState = {
  subj: [],
  courses: [],
  selected: [],
  newCourse: {},
  error: { type: "", message: "" },
  sections: [],
  selectedSection: null,
};

export const coursesReducer = (state, action) => {
  switch (action.type) {
    case "UPDATE_SUBJECTS": {
      return {
        ...state,
        subj: [...action.payload],
      };
    }
    case "UPDATE_COURSES": {
      return {
        ...state,
        courses: [...action.payload],
      };
    }
    case "UPDATE_SECTIONS": {
      return {
        ...state,
        sections: [...action.payload],
      };
    }
    case "UPDATE_SELECTED_SECTION": {
      return {
        ...state,
        selectedSection: action.payload,
      };
    }
    case "UPDATE_NEW_COURSE": {
      return {
        ...state,
        newCourse: { ...action.payload },
      };
    }
    case "UPDATE_SELECTED": {
      var temp = [...state.selected];
      temp.push({ ...action.payload });
      temp = temp.filter(
        (item, index, self) =>
          index ===
          self.findIndex(
            (t) =>
              t.crn === item.crn &&
              t.sub === item.sub &&
              t.crse === item.crse &&
              t.title === item.title
          )
      );
      return {
        ...state,
        selected: [...temp],
      };
    }
    case "REMOVE_FROM_SELECTED": {
      return {
        ...state,
        selected: state.selected.filter(
          (item) => item.crn !== action.payload.crn
        ),
      };
    }
    case "REMOVE_LAST_SELECTED": {
      return {
        ...state,
        selected: state.selected.filter(
          (item) => item.crn !== state.newCourse.crn
        ),
      };
    }
    case "ADD_TO_ERROR": {
      return {
        ...state,
        error: { ...action.payload },
      };
    }
    case "REMOVE_ERRORS": {
      return {
        ...state,
        error: { type: "", message: "" },
      };
    }

    default:
      return state;
  }
};
