import "bootstrap/dist/css/bootstrap.min.css";
import $ from "jquery";
import Popper from "popper.js";
import "bootstrap/dist/js/bootstrap.bundle.min";
import React from "react";
import ReactDOM from "react-dom";
import Application from "./application";
import "./app.css";
import GlobalContextProvider from "./context/GlobalContextProvider";

ReactDOM.render(
  <GlobalContextProvider>
    <Application />
  </GlobalContextProvider>,
  document.getElementById("app")
);
