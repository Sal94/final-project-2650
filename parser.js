const htmlparser2 = require("htmlparser2");
var clone = require('clone'); // to do deep copies of objects when pushing to final array
const fs = require('fs'); // used instead of axios as file needs to be cleaned before parsing and it's esier to just save things locally and work with that instead

let checkRow = false;
let isParagraph = false;
let isTd = false;
let rowCount = 0; // to check if element is abnormal from pattern
let whiteSpaceCounter = 0; // to check for multiline courses

let propertyToChange; // for inside loop, will be assigned to object.key

let objOutPut = [];

var prevCourse = {
    rp: " ",
    seatsAvailable: " ",
    waitlist: " ",
    sel: " ",
    crn: " ",
    subj: "",
    crse: " ",
    sec: " ",
    credit: " ",
    title: " ",
    addFees: " ",
    rpLimit: " ",
    type: [],
    day: [],
    time: [],
    nonStandardStart: [],
    nonStandardEnd: [],
    room: [],
    instructor: "bob "
}
var currCourse = {
    rp: " ",
    seatsAvailable: " ",
    waitlist: " ",
    sel: " ",
    crn: " ",
    subj: "",
    crse: " ",
    sec: " ",
    credit: " ",
    title: " ",
    addFees: " ",
    rpLimit: " ",
    type: [],
    day: [],
    time: [],
    nonStandardStart: [],
    nonStandardEnd: [],
    room: [],
    instructor: "sally "
}



const parser = new htmlparser2.Parser({
    onopentag(name, attribs) {
        if (name == "tr" && !attribs.bgcolor) {
            checkRow = true;
        }
        if (name == "p") {
            isParagraph = true;
        }
        if (name == "em") {
            isParagraph = false;
            isTd = false;
            checkRow = false;
        }
        if (name == "td") {
            if (!attribs.colspan) {
                isTd = true;
            }
            else {
                isTd = false;
                checkRow = false; //close the entire row
                currCourse = { // reset any data collected
                    rp: " ",
                    seatsAvailable: " ",
                    waitlist: " ",
                    sel: " ",
                    crn: " ",
                    subj: "",
                    crse: " ",
                    sec: " ",
                    credit: " ",
                    title: " ",
                    addFees: " ",
                    rpLimit: " ",
                    type: [],
                    day: [],
                    time: [],
                    nonStandardStart: [],
                    nonStandardEnd: [],
                    room: [],
                    instructor: "sally "
                }

            }
        }
    },
    ontext(text) {

        if (checkRow) {
            if (isTd || isParagraph) {
                if (text === " ") { // To check if multiline course
                    whiteSpaceCounter++;
                }
                if (rowCount >= 12 && rowCount <= 17) { // array properties
                    propertyToChange = Object.keys(currCourse)[rowCount];
                    currCourse[`${propertyToChange}`].push(text);
                }
                else { // normal properties
                    propertyToChange = Object.keys(currCourse)[rowCount];
                    currCourse[`${propertyToChange}`] = text;
                }
                rowCount++;

            }
        }
    },
    onclosetag(name) {
        if (name == "tr") {
            checkRow = false;
            if (rowCount < 19) { // if less than 19 rows then this data isn't relevant to courses
                rowCount = 0;
            }
            else {
                if (whiteSpaceCounter > 13) { // if whitespace counter greater than 13 then this is a multiline course , does not send object, instead combines properties and re runs
                    prevCourse['type'].push(currCourse['type']);
                    prevCourse['day'].push(currCourse['day']);
                    prevCourse['time'].push(currCourse['time']);
                    prevCourse['nonStandardStart'].push(currCourse['nonStandardStart']);
                    prevCourse['nonStandardEnd'].push(currCourse['nonStandardEnd']);
                    prevCourse['room'].push(currCourse['room']);
                    whiteSpaceCounter = 0;
                }
                else { // send prevcourse object and then overwrite it next iteration
                    prevCourse.type = [].concat.apply([], prevCourse.type); // flatten arrays, as they will become 2d or 3d depending on how many properties are pushed
                    prevCourse.day = [].concat.apply([], prevCourse.day);
                    prevCourse.time = [].concat.apply([], prevCourse.time);
                    prevCourse.nonStandardStart = [].concat.apply([], prevCourse.nonStandardStart);
                    prevCourse.nonStandardEnd = [].concat.apply([], prevCourse.nonStandardEnd);
                    prevCourse.room = [].concat.apply([], prevCourse.room);
                    var temp = clone(prevCourse); // deep copies object and sends it to final
                    objOutPut.push(temp);
                    prevCourse = clone(currCourse);
                    whiteSpaceCounter = 0;
                }
                rowCount = 0;
                currCourse = { // resets object
                    rp: " ",
                    seatsAvailable: " ",
                    waitlist: " ",
                    sel: " ",
                    crn: " ",
                    subj: "",
                    crse: " ",
                    sec: " ",
                    credit: " ",
                    title: " ",
                    addFees: " ",
                    rpLimit: " ",
                    type: [],
                    day: [],
                    time: [],
                    nonStandardStart: [],
                    nonStandardEnd: [],
                    room: [],
                    instructor: "sally "
                }


            }
        }
        if (name == "p") {
            isParagraph = false;
        }
        if (name == "td") {
            isTd = false;
        }
    }
}, { decodeEntities: true });

fs.readFile('cleanParsedCourseData.txt', 'utf-8', (err, data) => { // because were calling locally don't use axios and instead using fs
    if (err) throw err;
    console.log('File Read')
    parser.write(
        data
    );
    parser.end();
    fs.appendFile('courses.json', '{"data":[', err => { // create the json file
    })
    objOutPut.map(item => {
        fs.appendFile('courses.json', JSON.stringify(item) + ', \n', () => {})
    });
    fs.appendFile('courses.json', '{"end": "endOfFile"}]}', err => { // this will leave a trailing comma
    })
    fs.unlink('cleanParsedCourseData.txt', function(err) {
        if (err) throw err;
        console.log('File deleted!');
    });
})
